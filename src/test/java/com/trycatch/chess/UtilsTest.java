package com.trycatch.chess;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

public class UtilsTest {
	@Test
	public void testPermutations() throws Exception {
		Collection<List<String>> result = Utils.permutations(Arrays.asList("A", "B"));
		
		assertEquals(2, result.size());
		Iterator<List<String>> iter = result.iterator();
		assertEquals("[A, B]", iter.next().toString());
		assertEquals("[B, A]", iter.next().toString());
		
		result = Utils.permutations(Arrays.asList("A", "B", "C"));
		iter = result.iterator();
		
		assertEquals(6, result.size());
		assertEquals("[A, B, C]", iter.next().toString());
		assertEquals("[A, C, B]", iter.next().toString());
		assertEquals("[B, A, C]", iter.next().toString());
		assertEquals("[B, C, A]", iter.next().toString());
		assertEquals("[C, A, B]", iter.next().toString());
		assertEquals("[C, B, A]", iter.next().toString());
	}
}






