package com.trycatch.chess;

import static org.junit.Assert.*;

import org.junit.Test;

public class PositionTest {
	@Test
	public void testSameDiagonal() throws Exception {
		assertTrue(Position.at(3, 4).sameDiagonal(Position.at(6, 7)));
		assertTrue(Position.at(6, 7).sameDiagonal(Position.at(3, 4)));
		assertFalse(Position.at(6, 7).sameDiagonal(Position.at(4, 4)));
	}
	
	/* 1-based coordinates assumed (this is not so in real app, but makes no difference for test
	 *   1 2 3 4 5 6
	 * 1| | | | | | | 
	 * 2| | |*|*|*| | 
	 * 3| | |*|K|*| | 
	 * 4| | |*|*|*| | 
	 * 5| | | | | | | 
	 */
	@Test
	public void testWithinOneCellAround() throws Exception {
		assertTrue(Position.at(3, 4).withinOneCellAround(Position.at(2, 3)));
		assertTrue(Position.at(3, 4).withinOneCellAround(Position.at(2, 4)));
		assertTrue(Position.at(3, 4).withinOneCellAround(Position.at(2, 5)));
		assertTrue(Position.at(3, 4).withinOneCellAround(Position.at(3, 3)));
		assertTrue(Position.at(3, 4).withinOneCellAround(Position.at(3, 5)));
		assertTrue(Position.at(3, 4).withinOneCellAround(Position.at(4, 3)));
		assertTrue(Position.at(3, 4).withinOneCellAround(Position.at(4, 4)));
		assertTrue(Position.at(3, 4).withinOneCellAround(Position.at(4, 5)));
		
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(1, 2)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(1, 3)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(1, 4)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(1, 5)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(1, 6)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(2, 2)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(2, 6)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(3, 2)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(3, 6)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(4, 2)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(4, 6)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(5, 2)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(5, 3)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(5, 4)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(5, 5)));
		assertFalse(Position.at(3, 4).withinOneCellAround(Position.at(5, 6)));
	}
	
	@Test
	public void testSameRowOrCol() throws Exception {
		assertTrue(Position.at(3, 4).sameRowOrCol(Position.at(3, 3)));
		assertTrue(Position.at(3, 4).sameRowOrCol(Position.at(2, 4)));
		assertFalse(Position.at(3, 4).sameRowOrCol(Position.at(2, 3)));
	}
	
	/* 1-based coordinates assumed (this is not so in real app, but makes no difference for test
	 *   1 2 3 4 5 6
	 * 1| | |*| |*| | 
	 * 2| |*| | | |*| 
	 * 3| | | |N| | | 
	 * 4| |*| | | |*| 
	 * 5| | |*| |*| | 
	 */
	@Test
	public void testWithinKnightMoves() throws Exception {
		assertTrue(Position.at(3, 4).withinKnightMoves(Position.at(1, 3)));
		assertTrue(Position.at(3, 4).withinKnightMoves(Position.at(1, 5)));
		assertTrue(Position.at(3, 4).withinKnightMoves(Position.at(2, 2)));
		assertTrue(Position.at(3, 4).withinKnightMoves(Position.at(2, 6)));
		assertTrue(Position.at(3, 4).withinKnightMoves(Position.at(4, 2)));
		assertTrue(Position.at(3, 4).withinKnightMoves(Position.at(4, 6)));
		assertTrue(Position.at(3, 4).withinKnightMoves(Position.at(5, 3)));
		assertTrue(Position.at(3, 4).withinKnightMoves(Position.at(5, 5)));

		assertFalse(Position.at(3, 4).withinKnightMoves(Position.at(3, 2)));
		assertFalse(Position.at(3, 4).withinKnightMoves(Position.at(4, 3)));
	}
}
