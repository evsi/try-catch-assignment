package com.trycatch.chess;

import static org.junit.Assert.*;

import org.junit.Test;

import com.trycatch.chess.piece.ChessPiece;

public class ChessBoardTest {

	@Test
	public void placingInsideBoardIsAllowed() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertTrue(board.placeAt(ChessPiece.KING, Position.at(0, 0)));
		assertTrue(board.placeAt(ChessPiece.KING, Position.at(0, 1)));
		assertTrue(board.placeAt(ChessPiece.KING, Position.at(0, 2)));
		assertTrue(board.placeAt(ChessPiece.KING, Position.at(1, 0)));
		assertTrue(board.placeAt(ChessPiece.KING, Position.at(1, 1)));
		assertTrue(board.placeAt(ChessPiece.KING, Position.at(1, 2)));
	}
	
	@Test
	public void placingOutsideBoardIsNotAllowed() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertFalse(board.placeAt(ChessPiece.KING, Position.at(0, 3)));
	}
	
	@Test
	public void negativeCoordinatesAreNotAllowed() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertFalse(board.placeAt(ChessPiece.KING, Position.at(0, -1)));
	}
	
	@Test
	public void pieceCantBePlacedTwiceAtSamePosition() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertTrue(board.placeAt(ChessPiece.KING, Position.at(0, 1)));
		assertFalse(board.placeAt(ChessPiece.KNIGHT, Position.at(0, 1)));
	}
	
	@Test
	public void placingOutsideTheBoardIsNotAllowed() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertFalse(board.placeAt(ChessPiece.QUEEN, Position.at(-1, 0)));
		assertFalse(board.placeAt(ChessPiece.QUEEN, Position.at(3, 0)));
	}
	
	/*
	 *   0 1 2 
	 * 0|Q| | | 
	 * 1| | |Q| 
	 */
	@Test
	public void boardIsCheckedForPossibleConflicts() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertTrue(board.placeAt(ChessPiece.QUEEN, Position.at(0, 0)));
		assertFalse(board.canPlaceAt(ChessPiece.QUEEN, Position.at(0, 1)));
		assertFalse(board.canPlaceAt(ChessPiece.QUEEN, Position.at(0, 2)));
		assertFalse(board.canPlaceAt(ChessPiece.QUEEN, Position.at(1, 0)));
		assertFalse(board.canPlaceAt(ChessPiece.QUEEN, Position.at(1, 1)));
		assertTrue(board.canPlaceAt(ChessPiece.QUEEN, Position.at(1, 2)));
	}

	@Test
	public void pieceCantBePlacedOutsideTheBoard() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertFalse(board.canPlaceAt(ChessPiece.QUEEN, Position.at(-1, 0)));
		assertFalse(board.canPlaceAt(ChessPiece.QUEEN, Position.at(1, 3)));
	}
	
	/* Note that while Queen itself can't attack Knight in this position, Knight still can attack Queen
	 *   0 1 2 
	 * 0|N| | | 
	 * 1| | |Q| 
	 */
	@Test
	public void boardIsCheckedForPossibleConflictsWithDifferentPieces() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertTrue(board.placeAt(ChessPiece.KNIGHT, Position.at(0, 0)));
		assertFalse(board.canPlaceAt(ChessPiece.QUEEN, Position.at(0, 1)));
		assertFalse(board.canPlaceAt(ChessPiece.QUEEN, Position.at(0, 2)));
		assertFalse(board.canPlaceAt(ChessPiece.QUEEN, Position.at(1, 0)));
		assertFalse(board.canPlaceAt(ChessPiece.QUEEN, Position.at(1, 1)));
		assertFalse(board.canPlaceAt(ChessPiece.QUEEN, Position.at(1, 2)));
	}
	
	@Test
	public void nextAvailableWorksProperly() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertTrue(board.placeAt(ChessPiece.KNIGHT, Position.at(0, 1)));
		Position nextAvailable = board.nextAvailable(Position.at(0, 0));
		assertEquals(0, nextAvailable.row());
		assertEquals(2, nextAvailable.col());
	}

	@Test
	public void noNextAvailableAfterTheEndOfBoard() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertNull(board.nextAvailable(Position.at(1, 2)));
	}

	@Test
	public void noNextAvailableOutsideTheBoard() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertNull(board.nextAvailable(Position.at(-1, 0)));
	}
	
	@Test
	public void boardKeyIsGeneratedCorrectly() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertEquals("      ", board.generateKey());
		assertTrue(board.placeAt(ChessPiece.QUEEN, Position.at(0, 0)));
		assertTrue(board.placeAt(ChessPiece.ROOK, Position.at(0, 1)));
		assertTrue(board.placeAt(ChessPiece.BISHOP, Position.at(1, 0)));
		assertTrue(board.placeAt(ChessPiece.KNIGHT, Position.at(1, 1)));
		assertTrue(board.placeAt(ChessPiece.KING, Position.at(1, 2)));
		assertEquals("QR BNK", board.generateKey());
	}

	@Test
	public void toStringsPrintsTheBoard() throws Exception {
		ChessBoard board = new ChessBoard(2, 3);
		assertEquals("+-+-+-+\n" +
                     "| | | |\n" +
                     "+-+-+-+\n" +
                     "| | | |\n" +
                     "+-+-+-+\n",
                     board.toString());
		assertTrue(board.placeAt(ChessPiece.QUEEN, Position.at(0, 0)));
		assertTrue(board.placeAt(ChessPiece.ROOK, Position.at(0, 1)));
		assertTrue(board.placeAt(ChessPiece.BISHOP, Position.at(1, 0)));
		assertTrue(board.placeAt(ChessPiece.KNIGHT, Position.at(1, 1)));
		assertTrue(board.placeAt(ChessPiece.KING, Position.at(1, 2)));
		assertEquals("+-+-+-+\n" +
	                 "|Q|R| |\n" +
	                 "+-+-+-+\n" +
	                 "|B|N|K|\n" +
	                 "+-+-+-+\n",
	                 board.toString());
	}
}
