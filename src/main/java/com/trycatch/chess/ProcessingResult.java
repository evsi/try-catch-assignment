package com.trycatch.chess;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ProcessingResult {
	private Map<String, ChessBoard> boards = new ConcurrentHashMap<>();
	private AtomicInteger duplicates = new AtomicInteger(0);
	private AtomicInteger attempts = new AtomicInteger(0);
	private long startTime;
	private long endTime;
	
	public void start() {
		startTime = System.currentTimeMillis();
	}
	
	public void stop() {
		endTime = System.currentTimeMillis();
	}
	
	public long processingTime() {
		return endTime - startTime;
	}
	
	public ProcessingResult add(ChessBoard board) {
		attempts.incrementAndGet();
		
		String key = board.generateKey();
		ChessBoard previous = boards.putIfAbsent(key, board);

		if (previous != null) {
			duplicates.incrementAndGet();
		}
		
		return this;
	}

	public List<ChessBoard> boards() {
		return new ArrayList<>(boards.values());
	}
	
	public int duplicates() {
		return duplicates.get();
	}
	
	public int attempts() {
		return attempts.get();
	}
}
