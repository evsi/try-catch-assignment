package com.trycatch.chess.piece;

import com.trycatch.chess.Position;

public enum ChessPiece {
	BISHOP("B", (piecePosition, attackPosition) -> piecePosition.sameDiagonal(attackPosition)), 
	KING  ("K", (piecePosition, attackPosition) -> piecePosition.withinOneCellAround(attackPosition)), 
	KNIGHT("N", (piecePosition, attackPosition) -> piecePosition.withinKnightMoves(attackPosition)), 
	ROOK  ("R", (piecePosition, attackPosition) -> piecePosition.sameRowOrCol(attackPosition)), 
	QUEEN ("Q", (piecePosition, attackPosition) -> piecePosition.sameDiagonal(attackPosition) || piecePosition.sameRowOrCol(attackPosition)),
	;
	
	private String shortName;
	private AttackStrategy strategy;

	private ChessPiece(String shortName, AttackStrategy strategy) {
		this.shortName = shortName;
		this.strategy = strategy;
	}

	@Override
	public String toString() {
		return shortName;
	}

	public boolean canAttack(Position piecePosition, Position attackPosition) {
		return strategy.canAttack(piecePosition, attackPosition);
	}
	
	private static interface AttackStrategy {
		boolean canAttack(Position piecePosition, Position attackPosition);
	}
}
