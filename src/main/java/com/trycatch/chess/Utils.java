package com.trycatch.chess;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

public final class Utils {
	private Utils() {
	}

	public static <T> Collection<List<T>> permutations(List<T> source) {
		return buildPermutations(new LinkedHashSet<>(), new ArrayList<>(), source);
	}

	private static <T> Collection<List<T>> buildPermutations(Collection<List<T>> permutations, List<T> intermediate, List<T> source) {
		if (source.isEmpty()) {
			permutations.add(new ArrayList<>(intermediate));
			return permutations;
		}

		for (int i = 0; i < source.size(); i++) {
			List<T> newIntermediate = new ArrayList<>(intermediate);
			List<T> newSource = new ArrayList<>(source);
			newIntermediate.add(newSource.remove(i));
			buildPermutations(permutations, newIntermediate, newSource);
		}
		
		return permutations;
	}
}
