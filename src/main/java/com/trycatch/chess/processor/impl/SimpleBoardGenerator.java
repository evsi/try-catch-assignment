package com.trycatch.chess.processor.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.trycatch.chess.ChessBoard;
import com.trycatch.chess.Position;
import com.trycatch.chess.ProcessingResult;
import com.trycatch.chess.Utils;
import com.trycatch.chess.piece.ChessPiece;

public class SimpleBoardGenerator extends AbstractBoardGenerator {

	@Override
	public ProcessingResult generate(int rows, int cols, ChessPiece... pieces) {
		ProcessingResult result = new ProcessingResult();
		result.start();
		Collection<List<ChessPiece>> permutations = Utils.permutations(new ArrayList<>(Arrays.asList(pieces)));

		for (List<ChessPiece> permutation : permutations) {
			placePieces((board)-> result.add(board), new ChessBoard(rows, cols), permutation, Position.at(0, 0), 0);
		}
		result.stop();
		System.out.println();

		return result;
	}
}
