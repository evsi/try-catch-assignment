package com.trycatch.chess.processor.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.trycatch.chess.ChessBoard;
import com.trycatch.chess.Position;
import com.trycatch.chess.ProcessingResult;
import com.trycatch.chess.Utils;
import com.trycatch.chess.piece.ChessPiece;

public class ConcurrentBoardGenerator extends AbstractBoardGenerator {
	private long timeout;
	private TimeUnit unit;
	
	public ConcurrentBoardGenerator(long timeout, TimeUnit unit) {
		this.timeout = timeout;
		this.unit = unit;
	}

	@Override
	public ProcessingResult generate(int rows, int cols, ChessPiece... pieces) {
		ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		
		ProcessingResult result = new ProcessingResult();
		result.start();
		Collection<List<ChessPiece>> permutations = Utils.permutations(new ArrayList<>(Arrays.asList(pieces)));
		
		for (List<ChessPiece> permutation : permutations) {
			service.submit(() -> placePieces((board)-> result.add(board), new ChessBoard(rows, cols), permutation, Position.at(0, 0), 0));
		}
		service.shutdown();
		try {
			service.awaitTermination(timeout, unit);
		} catch (InterruptedException e) {
			return null;
		}
		result.stop();

		return result;
	}

}
