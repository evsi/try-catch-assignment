package com.trycatch.chess.processor.impl;

import java.util.List;

import com.trycatch.chess.ChessBoard;
import com.trycatch.chess.Position;
import com.trycatch.chess.piece.ChessPiece;
import com.trycatch.chess.processor.BoardGenerator;

public abstract class AbstractBoardGenerator implements BoardGenerator {

	protected void placePieces(BoardCollector collector, ChessBoard board, List<ChessPiece> permutation, Position startPos, int index) {
		if (index == permutation.size()) {
			collector.collect(board);
			return;
		}
		
		ChessPiece piece = permutation.get(index);
		Position pos = startPos;
		while(pos != null) {
			Position nextPos = board.nextAvailable(pos);
			if (board.canPlaceAt(piece, pos)) {
				ChessBoard newBoard = new ChessBoard(board);
				newBoard.placeAt(piece, pos);
				placePieces(collector, newBoard, permutation, nextPos, index + 1);
			}
			pos = nextPos;
		}
	}
	
	static interface BoardCollector {
		void collect(ChessBoard board);
	}
}
