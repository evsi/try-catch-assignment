package com.trycatch.chess.processor;

import com.trycatch.chess.ProcessingResult;
import com.trycatch.chess.piece.ChessPiece;

public interface BoardGenerator {
	ProcessingResult generate(int rows, int cols, ChessPiece... pieces);
}
