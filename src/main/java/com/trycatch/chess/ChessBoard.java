package com.trycatch.chess;

import com.trycatch.chess.piece.ChessPiece;

public class ChessBoard {
	private ChessPiece[] pieces;
	private int rows;
	private int cols;

	public ChessBoard(int rows, int cols) {
		this.rows = rows;
		this.cols = cols;
		pieces = new ChessPiece[rows*cols];
	}

	public ChessBoard(ChessBoard board) {
		this(board.rows, board.cols);
		System.arraycopy(board.pieces, 0, pieces, 0, pieces.length);
	}

	public boolean placeAt(ChessPiece piece, Position position) {
		if (isInvalidPosition(position)) {
			return false;
		}

		pieces[positionToOffset(position)] = piece;

		return true;
	}

	public boolean canPlaceAt(ChessPiece piece, Position position) {
		if (isInvalidPosition(position)) {
			return false;
		}

		for (int i = 0; i < pieces.length; i++) {
			if (pieces[i] == null) {
				continue;
			}
			ChessPiece existing = pieces[i];
			Position attackPosition = offsetToPosition(i);
			if (piece.canAttack(position, attackPosition) || existing.canAttack(attackPosition, position)) {
				return false;
			}
		}
		return true;
	}

	public Position nextAvailable(Position position) {
		if (outsideBoard(position)) {
			return null;
		}

		int start = positionToOffset(position) + 1;
		for (int i = start; i < pieces.length; i++) {
			if (pieces[i] == null) {
				return offsetToPosition(i);
			}
		}
		return null;
	}

	private Position offsetToPosition(int i) {
		return Position.at(i/cols, i % cols);
	}

	private int positionToOffset(Position position) {
		return position.row() * cols + position.col();
	}

	private ChessPiece get(Position position) {
		return pieces[positionToOffset(position)];
	}

	private boolean outsideBoard(Position position) {
		if (position.row() < 0 || position.col() < 0) {
			return true;
		}

		return (position.row() >= rows) || (position.col() >= cols);
	}

	private boolean isInvalidPosition(Position position) {
		return outsideBoard(position) || get(position) != null;
	}

	public String generateKey() {
		StringBuilder builder = new StringBuilder(rows * cols);
		forEach((position, piece) -> builder.append(piece == null ? " " : piece.toString()));
		return builder.toString();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder((rows * 2 + 1) * (cols * 2 + 1));
		forEach((position, piece) -> appendPiece(builder, position, piece));
		appendHorizontalDelimiter(builder);
		return builder.toString();
	}

	private void appendPiece(StringBuilder builder, Position position, ChessPiece piece) {
		if (position.col() == 0) {
			appendHorizontalDelimiter(builder);
		}

		builder.append('|').append(piece == null ? " " : piece.toString());

		if (position.col() == (cols - 1)) {
			builder.append('|').append('\n');
		}
	}

	private void appendHorizontalDelimiter(StringBuilder builder) {
		for (int i = 0; i < cols; i++) {
			builder.append("+-");
		}
		builder.append('+').append('\n');
	}

	private static interface Operation {
		void operation(Position position, ChessPiece piece);
	}

	void forEach(Operation operation) {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				Position position = Position.at(i, j);
				operation.operation(position, get(position));
			}
		}
	}
}
