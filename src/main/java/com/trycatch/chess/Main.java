package com.trycatch.chess;

import java.util.concurrent.TimeUnit;

import com.trycatch.chess.piece.ChessPiece;
import com.trycatch.chess.processor.BoardGenerator;
import com.trycatch.chess.processor.impl.ConcurrentBoardGenerator;

public class Main {
	public static void main(String[] args) {
		BoardGenerator generator = new ConcurrentBoardGenerator(1, TimeUnit.HOURS);
		
		ProcessingResult result = generator.generate(7, 7, 
				ChessPiece.KING, ChessPiece.KING, 
				ChessPiece.QUEEN, ChessPiece.QUEEN, 
				ChessPiece.BISHOP, ChessPiece.BISHOP, 
				ChessPiece.KNIGHT);

		System.out.println("Time      : " + timePrettyPrinter(result.processingTime()));
		System.out.println("Attempts  : " + result.attempts());
		System.out.println("Different : " + result.boards().size());
		System.out.println("Duplicates: " + result.duplicates());
		
		for (ChessBoard board : result.boards()) {
			System.out.println(board.toString());
		}
	}

	private static String timePrettyPrinter(long time) {
		if (time < TimeUnit.SECONDS.toMillis(1)) {
			return "" + time + "ms";
		}

		return "" + (time/1000) + "s";
	}
}
