package com.trycatch.chess;

import static java.lang.Math.abs;

public class Position {
	private final int row;
	private final int col;
	
	private Position(int row, int col) {
		this.row = row;
		this.col = col;
	}

	public static Position at(int row, int col) {
		return new Position(row, col);
	}

	public int row() {
		return row;
	}

	public int col() {
		return col;
	}
	
	public boolean sameDiagonal(Position attackPosition) {
		return abs(row() - attackPosition.row()) == abs(col() - attackPosition.col());
	}
	
	public boolean sameRowOrCol(Position attackPosition) {
		return row() == attackPosition.row() || col() == attackPosition.col();
	}
	
	public boolean withinOneCellAround(Position attackPosition) {
		return abs(row() - attackPosition.row()) <= 1 && abs(col() - attackPosition.col()) <= 1;
	}

	public boolean withinKnightMoves(Position attackPosition) {
		int rowOffset = Math.abs(row() - attackPosition.row());
		int colOffset = Math.abs(col() - attackPosition.col());
		return (rowOffset == 2 && colOffset == 1) || (rowOffset == 1 && colOffset == 2);
	}
}
